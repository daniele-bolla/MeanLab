/*Dependencies*/

var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
/*Socket */
io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('chat-message', function(msg){
    io.emit('new-message', msg)
    console.log(JSON.stringify(msg));
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});
var path = require('path');
var port = process.env.PORT || 8080;

var mongoose = require('mongoose');
var passport = require('passport');
var logger = require('morgan');
var flash = require('connect-flash');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var mongoStore = require('connect-mongo')(session);



/*Config*/

var config = require('./core/config/main');

/*Routes*/
var api = require('./core/routes/api');
var auth = require('./core/routes/auth')(passport);

/*mongo connection*/

mongoose.connect(config.db);
/*Register Models*/
//var User = require('./core/models/User.js');
//require('./core/models/Comments');

app.use(logger('dev'));
app.use(cookieParser())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

/*session*/
app.use(session({
  secret: 'soup',
  resave: false,
  saveUninitialized: false,
  store: new mongoStore({
    url: config.db,
    touchAfter: 24 * 3600 // time period in seconds 
  })
}));
/*Flask Message*/
app.use(flash());
/*passport init*/
app.use(passport.initialize());
app.use(passport.session());

/*Public dir*/
app.use('/', express.static(path.join(__dirname, 'public')));
/*Rest api*/
app.use('/api', api);
app.use('/auth', auth);
require('./core/config/passport')//var initPassport = require('./core/config/pass');
//initPassport(passport);

app.listen(port, function() {
  console.log('Example app listening on port ' + port);
});


/*Error Handler*/
/*
app.use(function(err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({
      "message": err.name + ": " + err.message
    });
  }
});
*/
