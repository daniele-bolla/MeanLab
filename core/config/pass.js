var User = require('../models/User');
var LocalStrategy = require('passport-local').Strategy;


module.exports = function(passport) {

  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  // used to deserialize the user
  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });

  // =========================================================================
  // LOCAL LOGIN =============================================================
  // =========================================================================
  passport.use('local-login', new LocalStrategy({
      usernameField: 'email',
      passReqToCallback: true
    },

    function(req, e, pass, done) {
      console.log(e);
      User.findOne({
        'email': e
      }, function(err, user) {
        if (err) return done(err);
        if (user) console.log(user);
        if (!user) return done(null, false,req.flash('message', "User not found, please try again or Signup if you don't have an account "));
        if (!user.validPassword(pass)) return done(null, false,req.flash('message', "Invalid password , please try again or follow the link below to recover your password"));
        return done(null, user);
      })
    }));
  // =========================================================================
  // LOCAL SIGNUP ============================================================
  // =========================================================================
  passport.use('local-signup', new LocalStrategy({
        usernameField: 'email',
        passReqToCallback: true // allows us to pass back the entire request to the callback
      },
      function(req, e, pass, done) {
        // find a user in mongo with provided username
        User.findOne({
          'email': e
        }, function(err, user) {
          // In case of any error, return using the done method
          if (err) {
            console.log('Error in SignUp: ' + err);
            return done(err);
          }
          // already exists
          if (user) {
            return done(null, false,req.flash('message', "User " + e + " already exists " ));
          } else {
            var u = new User({
              name: req.body.name,
              email: e,
            });
            u.password = u.generateHash(pass);
            u.save(function(err) {
              if (err) throw err;
              return done(null, u);
            });
            //console.log(u);
          }
        });
      })

  );
};