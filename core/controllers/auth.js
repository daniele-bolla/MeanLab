var passport = require('passport');
var mongoose = require('mongoose');
var User = require('../models/User');

var Auth = {

  register(req, res) {
    var query = User.findOne({
      email: req.body.email
    });
    query.exec(function(err, posts) {
      if (err) return res.status(500).json({message:err.message,errors:err.errors});
      var user = new User();
      user.name = req.body.name;
      user.email = req.body.email;
      user.setPassword(req.body.password);
      user.save(function(err) {
        if (err) {
          console.log(err);
          res.status(404).json({message:err.message,errors:err.properties.errors});
          return;
        }
        var token;
        token = user.generateJwt();
        res.status(200);
        res.json({
          message: "Signed Up !",
          token: token
        });
      });
    })
  },

  login(req, res) {
    passport.authenticate('local', function(err, user, info) {
      var token;
      console.log(1);
      // If Passport throws/catches an error
      if (err) {
        err = JSON.parse(err);
        res.status(404).json({message:err.message,errors:err.errors});
        return;
      }
      // If a user is found
      if (user) {
        token = user.generateJwt();
        res.status(200);
        res.json({
          message: "Signed In !",
          token: token
        });
      } else {
        console.log(4);
        // If user is not found
        res.status(401).json({
          message: info.message
        });
      }
    })(req, res);

  }
}

module.exports = Auth;