var mongoose = require('mongoose');
var User = mongoose.model('User');
var user = {
  hello(){
    return "hello"
  }
  list() {
    User.find({}, function(err, users) {
      var list = {};
      users.forEach(function(user) {
        list[user._id] = user;
      });
      return list;
    });
  }
}
var exports = module.exports = user;