var express = require('express');
var mongoose = require('mongoose');
var url  = require('url');
var fn = {
  
   urlArr(req, res) {
		var arr = url.parse(req.url, true);
	  arr = arr.path.split('/').splice(3, 4);
    res.json(arr);
  },
	
   hello(Model) {
		
		var arr =[];
    var query = Model.find();
		query.exec(function(err, users) {
			arr.push(users);
		});
		return arr
   /* Model.find({}, function(err, records) {
      console.log(err);
      var list = {};
      records.forEach(function(r) {
        list[r._id] = r;
      });
      return list;
    });*/
  },
  
  find(Model, param, value, select) {
    var query = Model.findOne({
      param: value
    });
    query.select(select);
    query.exec(function(err, find) {
      if (err) return err;
      return find;
    })
  },

  list(Model) {
    console.log(Model);
    Model.find({}, function(err, records) {
      console.log(err);
      var list = {};
      records.forEach(function(r) {
        list[r._id] = r;
      });
      return list;
    });
  }

}

module.exports = fn;