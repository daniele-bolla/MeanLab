var Ranks = {
  5 : "Client",
  10 : "Editor",
  50 : "Admin",
  100: "Super"
}

var isRank = {
  isClient(req, res, next) {
    if (req.isAuthenticated() && req.user.role >= 5) {
      next();
    } else {
      res.send('');
    }
  },

  isEditor(req, res, next) {
    if (req.isAuthenticated() && req.user.role >= 10) {
      next();
    } else {
      res.send('');
    }
  },

  isAdmin(req, res, next) {
    if (req.isAuthenticated() && req.user.role >= 50) {
      next();
    } else {
      res.send('');
    }
  },

  isSuper(req, res, next) {
    if (req.isAuthenticated() && req.user.role >= 100) {
      next();
    } else {
      res.send('');
    }

  }
}