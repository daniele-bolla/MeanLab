const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const commentSchema = mongoose.Schema({
	txt: String,
	post: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Post'
	},
	author: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	}
}, {
	timestamps: true
});

module.exports = mongoose.model('Comment', commentSchema);