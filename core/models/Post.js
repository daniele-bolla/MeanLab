const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const tagSchema = mongoose.Schema({
	name: {
		type: String,
	},
	posts: {}
});
const PostSchema = new Schema({
	tit: {
		type: String,
	},
	txt: {
		type: String,
	},
	slug: {
		type: String,
		trim: true
	},
	author: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	},
	comments: [{
		type: Schema.Types.ObjectId,
		ref: 'Comment'
	}],

	tags: [String]
}, {
	timestamps: true
});

module.exports = mongoose.model('Post', PostSchema);

PostSchema.pre('save', function(next) {
	var post = this;
	var tit = post.tit.toLowerCase();
	var slug = tit.replace(/[^a-zA-Z0-9]/ig, "-");
	post.slug = slug;
	next();
});