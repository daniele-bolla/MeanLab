const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const tagSchema = mongoose.Schema({
	name:  {
    type: String,
		unique:true
  },
	posts:[{
		type:Schema.Types.ObjectId,
		ref : 'Post'
	}]
});
module.exports = mongoose.model('Tag', tagSchema);