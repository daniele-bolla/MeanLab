const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const Schema = mongoose.Schema;
const userSchema = mongoose.Schema({
  email: {
    type: String,
    lowercase: [true, 'Must be lower case'],
    index: true,
    unique: [true, 'Email already taken'],
    trim: true,
    required: [true, 'Email is required'],
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address']
  },
  hash: String,
  salt: String,
  role: {
    type: Number,
    trim: true,
    enum: [5, 10, 50, 100],
    default: 5
  },
  name: {
    type: String,
    required: [true, 'Name is required'],
    trim: true,
  //  uppercase: true,
   // maxlength: [true, 'Name is too long'],
  },
  facebook: {
    id: String,
    token: String,
    email: String,
    name: String
  },
  twitter: {
    id: String,
    token: String,
    displayName: String,
    username: String
  },
  google: {
    id: String,
    token: String,
    email: String,
    name: String
  }

}, {
  timestamps: true
});
userSchema.methods.setPassword = function(password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
};
userSchema.methods.checkPassword = function(password) {
  var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
  return this.hash === hash;
};
userSchema.methods.generateJwt = function() {
  var expiry = new Date();
  expiry.setDate(expiry.getDate() + 7);

  return jwt.sign({
    _id: this._id,
    email: this.email,
    name: this.name,
    role: this.role,
    exp: parseInt(expiry.getTime() / 1000),
  }, "secret"); 
};

userSchema.methods.generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.password);
};


var ValidationError = mongoose.Error.ValidationError;
var ValidatorError  = mongoose.Error.ValidatorError;
var handle = function(err, doc, next) {
  if (err) {
    if (err.errors) {
      var arr = [];
      for (var v in err.errors) {
        arr.push(err.errors[v].message);
      }
      err.errors =  arr;
    }
    next(new ValidatorError (err));
  } else {
    next();
  }
}

userSchema.post('save', handle);
userSchema.post('update', handle);
userSchema.post('findOneAndUpdate', handle);
userSchema.post('insertMany', handle);

module.exports = mongoose.model('User', userSchema);