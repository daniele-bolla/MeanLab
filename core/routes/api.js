var express = require('express');
var router = express.Router();
var fn = require('../functions');
const mongoose = require('mongoose');
var User = require('../models/User');
var Post = require('../models/Post');
var Comment = require('../models/Comment');
var url = require('url');
var jwt = require('express-jwt');
var log = jwt({
  secret: 'secret',
  userProperty: 'payload'
});
//auth middleware
var logged = function(req, res, next) {
	if (req.log())
		return next();
	res.status(403).send({
		message: 'Unauthorized'
	});
}

// *************************************************************************
// RANKS LIST===============================================================
// *************************************************************************
router.get('/ranks', function(req, res) {
  var ranks = {
    5: "Client",
    10: "Editor",
    50: "Admin",
    100: "Super"
  }
  res.status(200).json(ranks);
});


  router.use('/posts', log);
  // *************************************************************************
  // POSTS API================================================================
  // *************************************************************************
router.route('/posts')
  //create
  .post(function(req, res) {
    if (req.body) {
      Post.findOne({
        'tit': req.body.tit
      }, function(err, find) {
        if (err)
          return res.status(500).json({
            message: JSON.stringify(err)
          });
        if (find) {
          return res.status(500).json({
            message: "Post already created with title : " + req.body.tit
          });
        } else {
          var post = new Post({
            tit: req.body.tit,
            txt: req.body.txt,
            author: req.body.user_id,
            tags: req.body.tags.replace(/\s/g, ' ').split(",")
          });
          post.save(function(err, post) {
            if (err) {
              return res.status(500).json({
                message: err
              });
            }
            return res.status(200).json({
              message: "Post Succesfult created"
            });
          });
        }
      })
    }
  })
  //get list
  .get(function(req, res) {
    var query = Post.find();
    query.populate({
      path: 'author',
      model: 'User',
    });
    query.exec(function(err, posts) {
      if (err) return res.status(500).json({
        message: err.message,
        errors: err.errors
      });
      return res.status(200).json(posts);
    })
  });
// *************************************************************************
// POSTS BY SLUG ===========================================================
// *************************************************************************
//set param
router.param('slug', function(req, res, next, id) {
  var query = Post.findOne({
    slug: req.params.slug
  });
  query.populate({
    path: 'comments',
    model: 'Comment',
    populate: {
      path: 'author',
      model: 'User',
    }
  });
  query.populate({
    path: 'author',
    model: 'User',
  });
  query.exec(function(err, post) {
    if (err) {
      return res.status(500).json({
        message: err.message,
        errors: err.errors
      });
    }
    if (!post) {
      return next(new Error('can\'t find post'));
    }
    req.post = post;
    return next();
  });
});
//api
router.route('/posts/:slug')
  // get post
  .get(function(req, res) {
    return res.json(req.post);
  })
  //set  post
  .put(function(req, res) {

  })
  //delete  post
  .delete(function(req, res) {

  });
// *************************************************************************
// SEARCH BY TAGS ==========================================================
// *************************************************************************
router.route('/posts/tags/*')
  .get(function(req, res) {
    var arr = url.parse(req.url, true);
    arr = arr.path.split('/').splice(3, 4);
    var query = Post.find({
      tags: {
        $in: arr
      }
    });
    query.exec(function(err, find) {
      if (err) return res.status(404).json({
        message: err.message,
        errors: err.errors
      });
      return res.json(find);
    })
  });
// *************************************************************************
// GET TAGS ================================================================
// *************************************************************************
router.route('/tags')
  .get(function(req, res) {
    var query = Post.find();
    var tags = [];
    query.exec(function(err, find) {
      if (err) return err;

      find.forEach(function(a) {
        if (a.tags) tags.push(a.tags);
      });
      if (typeof tags !== 'undefined' && tags.length > 0) {
        res.json(tags.reduce(function(a, b) {
          return a.concat(b);
        }))
      } else {
        return res.status(404).json({
          message: "No tasgs",
        });
      }
    })
  });
// *************************************************************************
// COMMENTS API ============================================================
// *************************************************************************
router.route('/posts/:slug/comments')
  .post(function(req, res) {
    var comment = new Comment({
      txt: req.body.txt,
      post: req.post,
      author: req.body.user_id
    });
    comment.save(function(err, comment) {
      if (err) {
        return next(err);
      }
      req.post.comments.push(comment);
      req.post.save(function(err, post) {
        if (err) {
          return next(err);
        }
        res.status(200).json({
          message: "Commented !"
        });
      });
    });
  });

module.exports = router;