var express = require('express');
var router = express.Router();
var User = require('../models/User');
var fn = require('../functions');
var auth = require('../controllers/auth');
var jwt = require('express-jwt');
var log = jwt({
  secret: 'MY_SECRET',
  userProperty: 'payload'
});
var logged = function(req, res, next) {
	if (req.log())
		return next();
	res.status(403).send({
		message: 'Unauthorized'
	});
}
module.exports = function(passport) {
	router.get('/test', function(req, res) {
		res.send('test');
	});
	// =========================================================================
	// JWT LOGIN ===============================================================
	// =========================================================================
	router.post("/login", auth.login);
	// =========================================================================
	// LOCAL LOGIN =============================================================
	// =========================================================================
	/*router.post("/login",
		passport.authenticate("local-login", {
			successRedirect: '/auth/loginSuccess',
			failureRedirect: "/auth/loginFailure",
			failureFlash: true
		}));
		*/
	router.get('/loginFailure', function(req, res) {
		res.status(401).json({
			message: req.flash('message')
		});
	});
	router.get('/loginSuccess', function(req, res) {
		res.status(200).json({
			message: 'Successfully Logged In',
			user: req.user
		});
	});
		// =======================================================================
	// JWT REGISTER ============================================================
	// =========================================================================
	router.post("/signup", auth.register);
	// =========================================================================
	// LOCAL REGISTER ==========================================================
	// =========================================================================
	/*router.post("/signup",
		passport.authenticate("local-signup", {
			successRedirect: '/auth/signupSuccess',
			failureRedirect: "/auth/signupFailure"
		}));*/
	router.get('/signupFailure', function(req, res) {
		res.json(403,{
			message: req.flash('message')
		});
	});
	router.get('/signupSuccess', function(req, res) {
		res.status(200).json({
			message: 'Successfully Signed Up and Logged In',
			user: req.user
		});
	});
	// =========================================================================
	// USERS-LIST ==============================================================
	// =========================================================================
	router.get("/users", log, function(req, res) {
		
		var query = User.find();
		query.exec(function(err, users) {
			res.send(users);
		});
	});
	// =========================================================================
	// LOGOUT ==================================================================
	// =========================================================================
	router.get("/logout", function(req, res) {
		req.logout();
		res.send("logout success!");
		res.redirect('/');
	});

	return router;

};