var gulp = require('gulp');
var ngAnnotate = require('gulp-ng-annotate');
var mainBowerFiles = require('gulp-main-bower-files');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var filter = require('gulp-filter');

gulp.task('d', function () {
 return gulp.src('./public/js/**/**/*.js')
    .pipe(concat('app.js'))
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(gulp.dest('public/dist'));
});

gulp.task('b-js', function(){
    return gulp.src('./bower.json')
        .pipe( mainBowerFiles())
        .pipe(filter('**/*.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/dist/libs/'));
});
gulp.task('b-css', function(){
    return gulp.src('./bower.json')
        .pipe( mainBowerFiles())
        .pipe(filter('**/*.css'))
        .pipe(uglify())
        .pipe(gulp.dest('public/dist/libs/'));
});
