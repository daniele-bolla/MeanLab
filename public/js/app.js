(function() {

  'use strict';  
  angular.module('app', ['ui.router'])

  .config(function($stateProvider, $urlRouterProvider, $locationProvider) {

    $locationProvider.html5Mode(true);

    $stateProvider
      .state('home', {
        url: '/home',
        view: 'home',
        onEnter: ['$state', 'jwt', function($state, jwt) {
          if (jwt.loggedIn()) {
            $state.go('posts');
          }
        }]
      }).state('chat', {
        url: '/chat',
        templateUrl: 'views/chat.html',
        controller: 'chat as s'
      }).state('login', {
        url: '/login',
        templateUrl: 'views/login.html',
        controller: 'auth as s'
      }).state('logout', {
        url: '/logout',
        controller: 'auth as s'
      }).state('register', {
        url: '/register',
        templateUrl: 'views/register.html',
        controller: 'auth as s'
      })
      .state('reset', {
        url: '/reset?token&email',
        templateUrl: 'view/resetpass.html',
        // controller: 'password as s'
      })
      .state('sendlink', {
        url: '/sendlink',
        templateUrl: 'views/sendlink.html',
        // controller: 'password as s'
      })
      .state('dashboard', {
        url: '/dashboard',
        templateUrl: 'views/dashboard.html',
        // controller: 'dashboard as s'
      })
      .state('profile', {
        url: '/dashboard/profile',
        templateUrl: 'views/dashboard.profile.html',
        //  controller: 'profile as s'
      }).state('posts', {
        url: '/posts',
        templateUrl: 'views/posts.html',
        controller: 'post as s'
      }).state('new', {
        url: '/posts/new',
        templateUrl: 'views/new.html',
        controller: 'post as s'
      }).state('post', {
        url: '/posts/:slug',
        templateUrl: 'views/post.html',
        controller: 'single as s'
      })

    $urlRouterProvider.otherwise('/home');

  }).config(function($httpProvider) {
    $httpProvider.interceptors.push(
      function($q, $location, jwt) {
        return {
          'request': function(config) {
            config.headers = config.headers || {};
            if (jwt.get()) {
              config.headers.Authorization = 'Bearer ' + jwt.get();
            }
            return config;
          },
          'response': function(res) {
            if (res.data.status === 401 || res.data.status === 500) {
              $location.path('/home');
              $location.replace();
            }
            return res || $q.when(res);
          }
        };
      }
    );
  });
})();