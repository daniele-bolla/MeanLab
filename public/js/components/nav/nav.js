(function() {
  'use strict';
  angular.module('navigation', [])
    .component('navigation', {
      templateUrl: 'js/components/nav/nav.tpl.html',
      $routeConfig: [
       {path: '/home/...', name: 'Home', component: 'home', useAsDefault: true},
        {path: '/login/...', name: 'Login', component: 'login' },    
        {path: '/register/...', name: 'Register', component: 'register' }
      ]
    });
})();