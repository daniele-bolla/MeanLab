(function() {
  'use strict';
  angular.module('app')
    .component('navbar', {
      templateUrl: 'js/components/navbar/navbar.tpl.html',
      controller: function(jwt, $state, $scope) {
        var vm = this;
        vm.u = jwt.user();
        vm.logged = jwt.loggedIn;
        vm.buttons = {
          logout: {
            color: "success",
            label: "Logout",
            icon: "sign-out",
            action: function() {
              jwt.destroy();
              $state.go('home');
            }
          }
        };
      }
    });
})();