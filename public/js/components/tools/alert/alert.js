(function() {
  'use strict';
  angular.module('app')
    .filter("isString", function() {
      return function(input) {
        return angular.isString(input);
      };
    })
    .filter("isArray", function() {
      return function(input) {
        return angular.isArray(input);
      };
    })
    .component('alert', {
      bindings: {
        data: '='
      },
      controller: function() {
        if(this.data){console.log(123)}
      },
      templateUrl: 'js/components/tools/alert/alert.tpl.html',


    })
})();