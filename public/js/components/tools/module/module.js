  (function() {
    'use strict';
    angular.module('app')
      .component('module', {
        bindings: {
          data: '<'
        },
        templateUrl: 'js/components/tools/module/module.tpl.html',

      });
  })();