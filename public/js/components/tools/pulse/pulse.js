(function() {
  'use strict';
  angular.module('app')
    .component('pulse', {
      bindings: {
        data: '<'
      },
      controller: function() {
        this.base = {
          color: 'success',
          alert: {},
          disabled: false,
          action: null
        };
      },
      templateUrl: 'js/components/tools/pulse/pulse.tpl.html'
    });
})();