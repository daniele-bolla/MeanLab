(function() {
  'use strict';
  angular.module('app')
    .component('usermenu', {
      templateUrl: 'js/components/usermenu/usermenu.tpl.html',
      controller: function(jwt) {
        var vm = this;
        vm.u = jwt.user();
        vm.buttons = {
          logout: {
            color: "success",
            icon: "comments-o",
            action: function() {
              alert()
            }
          }
        };
      }
    });
})();