(function() {
  'use strict';
  angular.module('app')
    .controller('auth', function($state, $http, jwt) {
      var s = this;

      s.formRegister = {
        name: 'register',
        title: 'Sign Up',
        color: 'success',
        action: function() {
          var self = this;
          $http.post('/auth/signup', s.formRegister.data).then(function(a) {
            jwt.save(a.data.token);
            self.alerts = {
              color: 'success',
              messages: a.data.message,
              icon: 'check'
            };
            $state.go('posts');
          }).catch(function(a) {
            
            self.alerts = {
              color: 'danger',
              messages: a.data.message,
              errors: a.data.errors,
              icon: 'exclamation'
            };
          });
        },
        fields: [{
          name: 'name',
          label: 'Name',
          type: 'text',
          required: true
        }, {
          name: 'email',
          label: 'Email',
          type: 'email',
          required: true
        }, {
          name: 'password',
          label: 'Password',
          type: 'password',
          required: true
        }, {
          name: 'password_confirm',
          label: 'Confirm Password',
          type: 'password',
          required: true
        }],
        submit: {
          label: 'Sign up',
          color: 'success',
          icon: 'fa fa-btn fa-sign-in',
        }
      };
      s.formLogin = {
        name: 'login',
        title: 'Sign In',
        color: 'success',
        action: function() {
          var self = this;
          $http.post('auth/login', s.formLogin.data).then(function(a) {
            jwt.save(a.data.token);
            self.alerts = {
              color: 'success',
              messages: a.data.message,
              icon: 'check'
            };
            $state.go('posts');
          }).catch(function(a) {
            self.alerts = {
              color: 'danger',
              messages: a.data.message,
              icon: 'exclamation'
            };
          });
        },
        fields: [{
          name: 'email',
          label: 'Email',
          type: 'email',
          required: true
        }, {
          name: 'password',
          label: 'Password',
          type: 'password',
          required: true
        }],
        buttons: [{
          name: 'recover',
          label: 'recover your pass',
          icon: 'key',
          color: 'primary'
        }],
        data: {
          email: "bolladaniele@gmail.com",
          password: "pass"
        },
        submit: {
          label: 'Sign in',
          color: 'success',
          icon: 'fa fa-btn fa-sign-in',
        }
      };

    })
})();