(function() {
  'use strict';
  angular.module('app')
    .controller('chat', function($scope, jwt) {
      var u = jwt.user();
      var s = this;
      s.messages = [];
      var socket = io.connect("http://104.155.84.112:3000/");
      socket.on("new-message", function(msg) {
        $scope.$apply(function() {
          console.log(msg);
          s.messages.push(msg);
        })

      })

      s.send = function(m) {
        console.log(m);
        var newmsg = {
          user: u.email,
          message: m
        }
        socket.emit('chat-message', newmsg);
        s.msg = '';
      }

    })
})();