(function() {
  'use strict';
  angular.module('app')
    .controller('post', function(ranks, $state, $http, jwt) {
    
      // *************************************************************************
      // LOGIC =================================================================
      // *************************************************************************

      var s = this;
      s.author = jwt.user();

      s.tagUrl = "api/posts/tags";

      s.searchByTags = function() {
        $http.get(s.tagUrl).then(function(a) {
          s.posts = a.data;
        });
      }

      s.addTags = function() {
        s.tagUrl += '/' + this.label;
        this.color = "success";
        console.log(s.tagUrl);
      }

      s.list = function() {
        $http.get('api/posts').then(function(a) {
          s.posts = a.data;
        });
      }
      s.tags = function() {
        $http.get('api/tags').then(function(a) {
          var arr = [];

          a.data.forEach(function(a) {
            arr.push({
              colr: "info",
              label: a,
              action: s.addTags
            });
            s.tags = arr;
          });
          console.log(s.tags);
        });
      }
      s.$onInit = s.list();
      s.$onInit = s.tags();
    
      // *************************************************************************
      // BUTTONS =================================================================
      // *************************************************************************

      s.buttons = {
        new: {
          color: "success",
          label: "Add New Post",
          icon: "plus",
          action: function() {
            $state.go('new');
          },
        },
        searchByTags: {
          color: "success",
          label: "Ok",
          icon: "thumbs-o-up",
          action: s.searchByTags,
        },

      };
    
      // *************************************************************************
      // FORM =================================================================
      // *************************************************************************

      s.formPost = {
        name: 'register',
        title: 'Add new Post',
        color: 'success',
        action: function() {
          var self = this;
          var param = {
            tit: s.formPost.data.tit,
            txt: s.formPost.data.txt,
            user_id: s.author._id,
            tags: s.formPost.data.tags
          }
          $http.post('/api/posts', param).then(function(a) {
            self.alerts = {
              color: 'success',
              messages: a.data.message,
              icon: 'check'
            };
            s.list();
          }).catch(function(a) {

            self.alerts = {
              color: 'danger',
              messages: a.data.message,
              icon: 'exclamation'
            };
          });
        },
        fields: [{
          name: 'tit',
          label: 'Title',
          type: 'text',
          required: true
        }, {
          name: 'tags',
          label: 'Tags',
          type: 'text',
          required: true,
          //list : ", "
        }],
        txts: [{
          name: 'txt',
          label: 'Text',
          type: 'text',
          required: true
        }],
        buttons: [{
          name: 'recover',
          label: 'recover your pass',
          icon: 'text',
          color: 'primary'
        }],
        submit: {
          label: 'Save',
          color: 'success',
          icon: 'fa fa-btn ',
        }
      };
    })
})();