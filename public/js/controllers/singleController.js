(function() {
  'use strict';
  angular.module('app')
    .controller('single', function( ranks, $state, $stateParams, $http, jwt) {
      var s = this;
      s.show = false;
      var author =  jwt.user();
      s.formComment = {
        name: 'comment',
        title: 'Add new Comment',
        color: 'success',
        action: function() {
          var self = this;
          var param = {
            txt: s.formComment.data.txt,
            user_id:author._id,
          }
          $http.post('/api/posts/'+ $stateParams.slug +'/comments', param).then(function(a) {
            self.alerts = {
              color: 'success',
              messages: a.data.message,
              icon: 'check'
            };
          }).catch(function(a) {
            self.alerts = {
              color: 'danger',
              messages: a.data.message,
              icon: 'exclamation'
            };
          });
        },
        txts: [{
          name: 'txt',
          label: 'Text',
          type: 'text',
          required: true
        }],
        submit: {
          label: 'Save',
          color: 'success',
          icon: 'fa fa-btn ',
        }
      };

      s.buttons = {
        com: {
          color: "success",
          icon: "comments-o",
          action: function() {
            s.show = true
          }
        }
      };

      s.$onInit = function() {
        $http.get('api/posts/' + $stateParams.slug).then(function(a) {
          s.post = a.data;
        });
      };

    })
})();