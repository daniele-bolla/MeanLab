(function() {
  'use strict';
  angular.module('app')
    .service('jwt', function(storage, $window) {
      var s = this;
      s.save = function(t) {
          storage.set("token", t);
      },
      s.get = function() {
        return storage.get("token");
      }
      s.parse = function(token) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse($window.atob(base64));
      }
      s.destroy = function() {
        storage.destroy("token");
      }
      s.loggedIn = function() {
        s.logged = (s.get()) ? s.parse(s.get()).exp > Date.now() / 1000 : false;
        return s.logged;
      }
      s.user = function() {
        if (s.get()) return s.parse(s.get());
      }
    })
})();