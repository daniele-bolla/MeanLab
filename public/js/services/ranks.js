(function() {
  'use strict';
  angular.module('app')
    .service('ranks', function($http) {
      var s = this;
      s.list = function(){
        return $http.get('/api/ranks').then(function(a){
          return a.data;
        })
      }
    })
})();