(function() {
  'use strict';
  angular.module('app')
    .service('storage', function() {
      var s = this;
      s.set = function(name, i) {
        localStorage.setItem(name, JSON.stringify(i));
      }
      s.get = function(name) {
        return JSON.parse(localStorage.getItem(name));
      }
      s.destroy = function(name) {
        localStorage.removeItem(name);
      }

    })
})();