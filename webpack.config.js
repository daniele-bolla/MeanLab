var HtmlWebpackPlugin = require('html-webpack-plugin');
var webpack = require('webpack');
var path = require('path');
module.exports = {
  entry: {
    app: "./public/js/index.js",
    vendor: "./public/vendor.js",
  },
  output: {
    path: __dirname + '/dist',
    filename: "bundle.js"
  },
  debug: true,
  watch:true,
  resolve: {
    modulesDirectories: ["web_modules", "node_modules", "./public/bower_components"]
  },
  module: {
    loaders: [{
      test: /[\/]angular\.js$/,
      loader: "exports?angular"
    }, {
      test: /\.css$/,
      loader: 'style!css'
    }, {
      test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/,
      loader: 'url-loader'
    }]
  },
  plugins: [
    new webpack.ResolverPlugin(
      new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin(".bower.json", ["main"])
    ),
    new HtmlWebpackPlugin({
      template: "./public/build.html",
      inject: "body"
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
    }),
  ]
};